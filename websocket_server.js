const WebSocket = require('ws');
require('./db/mongoose');
const { Message } = require('./models/message.model');

const wss = new WebSocket.Server({ host: "192.168.1.38",port: 8080 });

wss.on('connection', function connection(ws) {
  ws.send('Server is online. ●');
  ws.on('message', function incoming(message) {
    console.log('received: %s', message);
    const message1 = new Message({
      message_body: message,
      sender: 0,
      receiver: 1
    });
    message1.save().then((m) => {
     console.log('Message saved.');
     ws.send('Your message: ' + message + ' Saved at: ' + m.timestamp);
    });
  });

});
