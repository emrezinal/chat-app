const mongoose = require('mongoose');

const messageSchema = new mongoose.Schema({
    message_body: { type: String, required: true },
    sender : { type: Number, required: true }, // TODO: mongoose.Schema.Types.ObjectId
    receiver : { type: Number, required: true }, // TODO: mongoose.Schema.Types.ObjectId
    timestamp : { type : Date, default: Date.now }
});

const Message = mongoose.model("Message", messageSchema);

module.exports = { Message };
